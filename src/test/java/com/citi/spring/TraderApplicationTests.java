package com.citi.spring;

import java.util.Date;

import com.citi.spring.entities.Trade;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class TraderApplicationTests {

	@Test
	void contextLoads() {
	}


	@Test public void testTrade() {
		//arrange
		Date myDate = new Date();
		String tickerTest = "AMZNTest";
		int quantityTest = 4;
		String amountTest = "500";
		String typeTest = "CREATED";

		//act
		Trade test = new Trade(myDate, tickerTest, quantityTest, amountTest, typeTest);

		//assert
		assert(test.getDateCreated().equals(myDate));
		assert(test.getTicker().equals(tickerTest));
		assert(test.getQuantity() == quantityTest);
		assert(test.getAmount().equals(amountTest));
		assert(test.getTradeType().equals(typeTest));

	}

	@Test public void testDate(){
		Date myDate = new Date();
		Trade test = new Trade();
		test.setDateCreated(myDate);
		Date testDate = test.getDateCreated();
		assert myDate.equals(testDate);
	}


	@Test public void testTicker() {
		//Setup
		String giveTicker = "beans";
		//Given
		Trade test = new Trade();
		test.setTicker(giveTicker);
		//When
		String testTicker = test.getTicker();
		//Then
		assert giveTicker.equals(testTicker);
	}

	/*@Test public void testState() {
		//Setup
		String giveState = "CREATED";
		//Given
		Trade test = new Trade();
		test.setState(giveState);
		//When
		String testState = test.getState();
		//Then
		assert giveState.equals(testState);
	}*/

	@Test public void testType() {
		//Setup
		String giveType = "BUY";
		//Given
		Trade test = new Trade();
		test.setTradeType(giveType);
		//When
		String testType = test.getTradeType();
		//Then
		assert giveType.equals(testType);
	}

	@Test public void testQuantity() {
		//Setup
		int giveQuantity = 4;
		//Given
		Trade test = new Trade();
		test.setQuantity(giveQuantity);
		//When
		int testQuantity = test.getQuantity();
		//Then
		assert giveQuantity == testQuantity;
	}

	@Test public void testAmount() {
		//Setup
		String giveAmount = "500";
		//Given
		Trade test = new Trade();
		test.setAmount(giveAmount);
		//When
		String testAmount = test.getAmount();
		//Then
		assert giveAmount.equals(testAmount);
	}


	

}
