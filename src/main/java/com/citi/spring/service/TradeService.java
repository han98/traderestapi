package com.citi.spring.service;

import com.citi.spring.data.TradeRepository;
import com.citi.spring.entities.Trade;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public class TradeService {
    @Autowired
    private TradeRepository dao;

    public Collection<Trade> getAll() { return dao.findAll(); }

    public Optional<Trade> getTradeById(ObjectId id){
        return dao.findById(id);
	}

    public void deleteTrade(ObjectId id) {
        dao.deleteById(id);
    }

    public void createTrade(Trade t){
        t.setState("CREATED");
        dao.insert(t);
    }

    /*public void updateTrade(ObjectId id, Trade trade) {
        Optional<Trade> tradeData = dao.findById(id);

        if (tradeData.isPresent()) {
            Trade updateTrade = tradeData.get();
            updateTrade.setDateCreated(trade.getDateCreated());
            updateTrade.setTicker(trade.getTicker());
            updateTrade.setQuantity(trade.getQuantity());
            updateTrade.setAmount(trade.getAmount());
            updateTrade.setTradeType(trade.getTradeType());
            updateTrade.setState(trade.getState());
            dao.save(updateTrade);
        }

    }*/
}
