package com.citi.spring.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
public class Trade {

    @Id
    private String _id;
    private Date dateCreated;
    private String ticker;
    private int quantity;
    private String amount;
    private String tradeType;
    private String state;

    public Trade(){}

    public Trade(Date dateCreated, String ticker, int quantity, String amount, String tradeType){
        this.dateCreated = dateCreated;
        this.ticker = ticker;
        this.quantity = quantity;
        this.amount = amount;
        this.tradeType = tradeType;
        this.state = "CREATED";
    }

    public String getId(){
        return _id;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTradeType() {
        return tradeType;
    }

    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

}
